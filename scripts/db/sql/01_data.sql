--
-- PostgreSQL database dump
--

-- Dumped from database version 11.3
-- Dumped by pg_dump version 11.3

-- Started on 2019-11-28 03:24:29

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2833 (class 0 OID 16575)
-- Dependencies: 199
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."user" VALUES (1, 'bachir.rabaaoui', 'bachir.rabaaoui@gmail.com', 'f71dbe52628a3f83a77ab494817525c6', 'Bachir', 'Rabaaoui', NULL);
INSERT INTO public."user" VALUES (2, 'john.smith', 'john.smith@gmail.com', 'acbd18db4cc2f85cedef654fccc4a4d8', 'John', 'Smith', NULL);
INSERT INTO public."user" VALUES (3, 'anna.johnson','anna.johnson@gmail.com', 'acbd18db4cc2f85cedef654fccc4a4d8', 'Anna', 'Johnson', NULL);
INSERT INTO public."user" VALUES (4, 'paul.graham', 'paul.graham@gmail.com', 'acbd18db4cc2f85cedef654fccc4a4d8', 'Paul', 'Graham', NULL);
INSERT INTO public."user" VALUES (5, 'elon.musk', 'elon.musk@gmail.com', 'acbd18db4cc2f85cedef654fccc4a4d8', 'Elon', 'Musk', NULL);


--
-- TOC entry 2831 (class 0 OID 16565)
-- Dependencies: 197
-- Data for Name: project; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.project VALUES (1, 'My cool project', 'This is a cool project, indeed.', 1);
INSERT INTO public.project VALUES (2, 'Sablier App', 'Hassle-free time management app.', 1);
INSERT INTO public.project VALUES (3, 'Cybertruck', 'The new truck by Tesla.', 5);


--
-- TOC entry 2835 (class 0 OID 16585)
-- Dependencies: 201
-- Data for Name: timesheet; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.timesheet VALUES (1, 'Create a script to purge the database', NULL, '2019-11-24 12:00:00+01', 3600, NULL, 1, 1);
INSERT INTO public.timesheet VALUES (2, 'Add new security roles', NULL, '2019-11-24 10:00:00+01', 2700, NULL, 1, 1);
INSERT INTO public.timesheet VALUES (3, 'Add "About me" page to the website', NULL, '2019-11-27 12:00:00+01', 7200, NULL, 1, 1);
INSERT INTO public.timesheet VALUES (4, 'Brainstorming to find a refreshing name for the project.', NULL, '2019-11-21 12:00:00+01', 900, NULL, 1, 1);


--
-- TOC entry 2841 (class 0 OID 0)
-- Dependencies: 196
-- Name: project_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.project_id_seq', 3, true);


--
-- TOC entry 2842 (class 0 OID 0)
-- Dependencies: 200
-- Name: timesheet_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.timesheet_id_seq', 4, true);


--
-- TOC entry 2843 (class 0 OID 0)
-- Dependencies: 198
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_id_seq', 5, true);


-- Completed on 2019-11-28 03:24:30

--
-- PostgreSQL database dump complete
--

