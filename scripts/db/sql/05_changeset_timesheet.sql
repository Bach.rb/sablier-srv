ALTER TABLE public.timesheet ADD COLUMN taskId integer;

ALTER TABLE public.timesheet 
ADD CONSTRAINT fk_timesheet_task FOREIGN KEY (taskId) REFERENCES public.task (id);