/*
Ajout contrainte unique sur le champ email  
*/
ALTER TABLE public.user
ADD CONSTRAINT u_user_email UNIQUE (email);

/*
Ajout contrainte unique sur le champ username  
*/
ALTER TABLE public.user
ADD CONSTRAINT u_user_username UNIQUE (username);

/*
Ajout champ salt dans user 
*/
ALTER TABLE public.user
ADD COLUMN salt text;
