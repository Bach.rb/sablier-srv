var SequelizeAuto = require('sequelize-auto')
var auto = new SequelizeAuto('sablierdb', 'postgres', '123456', {
    host: '127.0.0.1',
    dialect: 'postgres', 
    port: '5432',
    directory: './scripts/db/models/',
    additional: {
        timestamps: false
    }
})

auto.run(function (err) {
    if (err) throw err;
  
    console.log('Génération');
    console.log(auto.tables); // table list
    console.log(auto.foreignKeys); // foreign key list
  });