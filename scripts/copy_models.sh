#!/bin/bash
# Script de copie des modèles générés par sequelize-auto 

src_models_folder="./scripts/db/models/*"; 
echo "Start copying models from ($src_models_folder)..."

for filename in ./scripts/db/models/*;
do
   str_filename=`basename "$filename"`; 
   str_filename_replaced=`echo $str_filename | sed "s/\.js//g"`
   mkdir -p "src/components/$str_filename_replaced" && cp "./scripts/db/models/$str_filename" "src/components/$str_filename_replaced/model.js" && echo "Generated model : $str_filename_replaced at : src/components/$str_filename_replaced/model.js"
done;

echo "Done with generating models";