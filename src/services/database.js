const Sequelize = require('sequelize');
var sequelize = null;

module.exports = {
    init: function (req, res, next) {
        if (sequelize) {
            res.locals.db = sequelize;
            next();
        }
        else {
            sequelize = new Sequelize('sablierdb', 'postgres', '123456', {
                host: '127.0.0.1',
                dialect: 'postgres',
                port: '5432',
                additional: {
                    timestamps: false
                }
            });
            sequelize
                .authenticate()
                .then(() => {
                    console.log('Connection has been established successfully.');
                    sequelize.import('../components/user/model');
                    sequelize.import('../components/timesheet/model');
                    sequelize.import('../components/project/model');
                    sequelize.import('../components/task/model');
                    // Timesheet
                    sequelize.models.timesheet.belongsTo(sequelize.models.user, { foreignKey: 'authorid', targetKey: 'id' });
                    sequelize.models.user.hasMany(sequelize.models.timesheet, { foreignKey: 'authorid', sourceKey: 'id' });
                    // Task
                    // sequelize.models.task.belongsTo(sequelize.models.user, { foreignKey: 'authorid', targetKey: 'id' });
                    // sequelize.models.user.hasMany(sequelize.models.timesheet, { foreignKey: 'authorid', sourceKey: 'id' });
                    res.locals.db = sequelize;

                    next();
                })
                .catch(err => {
                    console.error('Unable to connect to the database:', err);
                    sequelize = null;
                });
        }
    }
}
