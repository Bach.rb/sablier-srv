var jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const saltRounds = 10;
//var db = require('../../scripts/db/models');


module.exports = {
    // login: function (req, res, next) {
    //     var username = req.body.username;
    //     var passwd = req.body.encryptedPasswd;
    //     db.models.user.findOne({ where: { username: username, passwd: passwd } })
    //         .then(user => {
    //             if (user) {
    //                 var content = module.exports.getJWTContent(user);
    //                 var options = module.exports.getJWToptions();
    //                 jwt.sign(content, 'myKey', options, function (err, token) {
    //                     if (err) throw err;
    //                     res.cookie('sessid', token, /* { secure: true, httpOnly: true } */);
    //                     next();
    //                 });
    //             }
    //             else
    //                 res.send('Username not found');
    //         })
    //         .catch(err => {
    //             res.send(err);
    //         });
    // },
    getToken2: function (req) {
        if (req.cookies && req.cookies.sessid)
            return req.cookies.sessid;
        else
            return null;
    },
    getToken: function (user, next) {
        var payload = module.exports.getJWTPayload(user);
        var options = module.exports.getJWToptions();
        jwt.sign(payload, 'myKey', options, next);
    },
    checkToken: function (req, res, next) {
        jwt.verify(req.cookies.sessid, 'myKey', function (err, decoded) {
            if (err) next(new Error('Authentication failed'));
            else {
                res.locals.currentUserId = decoded.id;
                next();
            }
        });
    },
    getJWTPayload: function (user) {
        return {
            id: user.id,
            username: user.username,
            firstName: user.firstname,
            lastName: user.lastname
        }
    },
    getJWToptions: function () {
        return {
            expiresIn: "1d"
        };
    },
    hashPassword: function (passwd, next) {
        bcrypt.genSalt(saltRounds, function (err, salt) {
            bcrypt.hash(passwd, salt, function (err, hash) {
                var hashData = {
                    hash: hash,
                    salt: salt
                };
                next(hashData);
            });
        });
    },
    checkPassword: function (passwd, hash) {
        return bcrypt.compare(passwd, hash);
    },
    getCurrentUsername: function (sessionCookie, next) {
        jwt.verify(sessionCookie, 'myKey', function (err, decoded) {
            next(decoded.id)
        });
    }


}

