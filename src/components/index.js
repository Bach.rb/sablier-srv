var express = require('express');
var router = express.Router();
var auth = require('../services/auth.js');

// Middlewares
router.use(auth.checkToken);

router.use("/project", require('./project'));
router.use("/project/:projectId/task", require('./task'));
router.use("/project/:projectId/timesheet", require('./timesheet'));
router.use("/user", require('./user'));



module.exports = router;