var express = require('express');
var router = express.Router({ mergeParams: true });
var controller = require('./controller');

// Middleware
router.use(controller.loadModel);

// Get all timesheets 
router.get('/', function (req, res, next) {
    if (req.params.projectId) {
        controller.getTimesheets(res.locals.currentUserId, req.params.projectId)
            .then(result => {
                res.status(200).send(result);
            })
            .catch(err => next(new Error('Error when retrieving timesheets')));
    }
    else
        next(new Error('Error when retrieving Timesheets'));
});

// Get specific timesheet 
router.get('/:timesheetId', function (req, res, next) {
    if (req.params.projectId && req.params.timesheetId) {
        controller.getTimesheet(res.locals.currentUserId, req.params.projectId, req.params.timesheetId)
            .then(result => {
                res.status(200).send(result);
            })
            .catch(err => next(new Error('Error when retrieving timesheets')));
    }
    else
        next(new Error('Error when retrieving Timesheets'));
});

// Create timesheet 
router.put('/', function (req, res, next) {
    if (req.body.data && req.params.projectId) {
        var data = JSON.parse(req.body.data);
        controller.addNewTimesheet(res.locals.currentUserId, req.params.projectId, data)
            .then(() => {
                res.status(200).send('Added');
            })
            .catch(err => next(new Error('Error when creating timesheet')));
    }
    else
        next(new Error('Error when creating timesheet'));
});

// Update timesheet 
router.patch('/:timesheetId', function (req, res, next) {
    if (req.params.projectId && req.params.timesheetId && req.body.data) {
        var data = JSON.parse(req.body.data);
        controller.updateTimesheet(res.locals.currentUserId, req.params.projectId, req.params.timesheetId, data)
            .then(() => {
                res.status(200).send('Updated');
            })
            .catch(err => next(new Error('Error when updating timesheet')));
    }
    else
        next(new Error('Error when updating timesheet'));
});

// Delete timesheet 
router.delete('/:timesheetId', function (req, res, next) {
    if (req.params.projectId && req.params.timesheetId) {
        controller.deleteTimesheet(res.locals.currentUserId, req.params.projectId, req.params.timesheetId)
            .then(() => {
                res.status(200).send('Deleted');
            })
            .catch(err => next(new Error('Error when deleting timesheet')));
    }
    else
        next(new Error('Error when deleting timesheet'));
});


module.exports = router;