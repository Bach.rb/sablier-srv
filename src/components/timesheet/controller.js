var db = null;

module.exports = {
    loadModel: function (req, res, next) {
        //res.locals.db.import('./model');
        if (!db) db = res.locals.db.models.timesheet;
        next();
    },
    getTimesheets: function (authorId, projectId) {
        return db.findAll({ where: { authorid: authorId, projectid: projectId } })
    },
    getTimesheet: function (authorId, projectId, timesheetId) {
        return db.findOne({ where: { authorid: authorId, projectid: projectId, id:timesheetId } })
    },
    updateTimesheet: function (authorId, projectId, timesheetId, data) {
        return db.update({
            title: data.title,
            description: data.description,
        }, { where: { id: timesheetId, authorid: authorId, projectid: projectId } });
    }, 
    addNewTimesheet: function (authorId, projectId, data) {
        console.log(data);
        return db.create({
            title: data.title,
            description: data.description,
            startingdate: data.startingDate,
            endingdate: data.endingDate,
            duration: data.duration,
            authorid: authorId, 
            projectid: projectId
            // TODO : add taskId
        });
    }, 
    deleteTimesheet: function (authorId, projectId, timesheetId) {
        return db.destroy({ where: { authorid: authorId,  id: timesheetId, projectid: projectId } }); 
    }, 
    // getUserTimesheet: function (username, next) {
    //     db.findAll({
    //         include: [{
    //             model: db.models.user,
    //             where: {
    //                 username: username
    //             }
    //         }]
    //     })
    //         .then(results => {
    //             next(results)
    //         })
    //         .catch(() => {
    //             next(null);
    //         })
    // },
    // createTimesheet: function (userId, projectId, data) {
    //     return db.create({
    //         title: data.title,
    //         description: data.description,
    //         startingdate: data.startingDate,
    //         duration: data.duration,
    //         endingdate: data.endingDate,
    //         projectid: projectId,
    //         authorid: userId
    //     });
    // }
}

