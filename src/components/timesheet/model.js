/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('timesheet', {
		id: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true, 
			autoIncrement: true
			
		},
		title: {
			type: DataTypes.TEXT,
			allowNull: true
		},
		description: {
			type: DataTypes.TEXT,
			allowNull: true
		},
		startingdate: {
			type: DataTypes.DATE,
			allowNull: true
		},
		duration: {
			type: DataTypes.INTEGER,
			allowNull: true
		},
		endingdate: {
			type: DataTypes.DATE,
			allowNull: true
		},
		projectid: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'project',
				key: 'id'
			}
		},
		authorid: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'user',
				key: 'id'
			}
		}
	}, {
		tableName: 'timesheet',
		timestamps: false
	});
};
