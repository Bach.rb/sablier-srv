var db = null;

module.exports = {
    loadModel: function (req, res, next) {
        if (!db) db = res.locals.db.models.project;
        next();
    },
    getProjects: function (authorId) {
        return db.findAll({ where: { authorid: authorId } })
    },
    getProject: function (authorId, projectId) {
        return db.findOne({ where: { id: projectId, authorid: authorId } })
    },
    updateProject: function (authorId, projectId, data) {
        return db.update({
            title: data.title,
            description: data.description,
            authorId: authorId
        }, { where: { id: projectId, authorid: authorId } });
    },
    addNewProject: function (authorId, data) {
        return db.create({
            title: data.title,
            description: data.description,
            authorid: authorId
        });
    }, 
    deleteProject: function(authorId, projectId) {
        return db.destroy({ where: { authorid: authorId, id: projectId } }); 
        
    } 
}

