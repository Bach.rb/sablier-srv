var express = require('express');
var router = express.Router();
var controller = require('./controller');

// Middleware
router.use(controller.loadModel);

// Récupérer tous les projets 
router.get('/', function (req, res, next) {
    controller.getProjects(res.locals.currentUserId)
    .then(result => {
        res.status(200).send(result);
    })
    .catch(err => next(new Error('Error when retrieving projects')));
    
});

// Récupérer un projet
router.get('/:projectId', function (req, res, next) {
    if (req.params.projectId) {
        controller.getProject(res.locals.currentUserId, req.params.projectId)
        .then(result => {
            res.status(200).send(result);
        })
        .catch(err => next(new Error('Error when retrieving projects')));
    }
    else 
        next(new Error('Error when retrieving projects'));
});

// Ajouter un projet 
router.put('/', function (req, res, next) {
    if (req.body.data) {
        var data = JSON.parse(req.body.data);
        controller.addNewProject(res.locals.currentUserId, data)
        .then(() => {
            res.status(200).send('OK');
        })
        .catch(err => next(new Error('Error when retrieving projects')));
    }
    else 
        next(new Error('Error when creating project'));
});


// Modifier un projet
router.patch('/:projectId', function (req, res, next) {
    if (req.params.projectId && req.body.data) {
        var data = JSON.parse(req.body.data);
        controller.updateProject(res.locals.currentUserId, req.params.projectId, data)
        .then(() => {
            res.status(200).send('OK');
        })
        .catch(err => next(new Error('Error when retrieving projects')));
    }
    else 
        next(new Error('Error when creating project'));
});

// Supprimer un projet
router.delete('/:projectId', function (req, res, next) {
    if (req.params.projectId) {
        controller.deleteProject(res.locals.currentUserId, req.params.projectId)
        .then(() => {
            res.status(200).send('OK');
        })
        .catch(err => next(new Error('Error when deleting projects')));
    }
});

// router.use( function (req, res, next) {
//     res.send(res.locals.data)
//  }); 


module.exports = router;