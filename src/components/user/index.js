var express = require('express');
var router = express.Router();
var controller = require('./controller');
var auth = require('../../services/auth.js')

// Middleware
router.use(controller.loadModel);

router.get('/get/:username', function (req, res, next) {
    res.locals.db.models.user.findOne({ where: { username: req.params.username } })
        .then(user => {
            res.send(user);
        })
        .catch(err => res.send(err));
});

router.post('/add', function (req, res, next) {
    var email = req.body.email;
    var passwd = req.body.passwd;
    if (email && passwd) {
        controller.createUser(email, passwd, function (err) {
            if (err)
                res.send('Error while creating the user : ' + err);
            else
                res.send('User created');
        })
    }
    else
        res.send('Missing parameters');
});

router.put('/update/:id',function (req, res, next) {
    var id = req.params.id;
    var userdata = req.body.userdata;
    if (id && userdata) {
        controller.updateUser(id, userdata, function (err) {
            if (err)
                res.send('Error while updating the user : ' + err);
            else
                res.send('User updated');
        })
    }
    else
        res.send('Missing parameters');
});



router.delete('/delete/:id', function (req, res, next) {
    var id = req.params.id;
    if (id) {
        controller.deleteUser(id, function (err) {
            if (err)
                res.send('Error while deleting the user : ' + err);
            else
                res.send('User deleted');
        })
    }
    else
        res.send('Missing parameters');
});

module.exports = router;