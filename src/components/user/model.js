/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('user', {
		id: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true, 
			autoIncrement: true
		},
		username: {
			type: DataTypes.TEXT,
			allowNull: true,
			unique: true
		},
		email: {
			type: DataTypes.TEXT,
			allowNull: true,
			unique: true
		},
		passwd: {
			type: DataTypes.TEXT,
			allowNull: true
		},
		firstname: {
			type: DataTypes.TEXT,
			allowNull: true
		},
		lastname: {
			type: DataTypes.TEXT,
			allowNull: true
		},
		profilepic: {
			type: DataTypes.TEXT,
			allowNull: true
		},
		salt: {
			type: DataTypes.TEXT,
			allowNull: true
		}
	}, {
		tableName: 'user',
		timestamps: false
	});
};
