var db = null;

module.exports = {
    loadModel: function (req, res, next) {
        res.locals.db.import('./model');
        if (!db) db = res.locals.db;
        next();
    },
    checkUserExists: function (username, password) {
        model.findOne({ where: { username: username, passwd: passwd } })
            .then(user => {
                return user;
            })
            .catch(err => {
                return null;
            });
    },
    getUserById: function (id) {
        return null;
    },
    /*
    * Create the user
    */
    createUser: function (email, password, next) {
        if (email && password) {
            db.models.user.create({ email: email, passwd: password }).then(newUser => {
                next(null);
            }).catch(err => {
                next(err);
            });
        }
    },
    /*
    * Update the user 
    */
    updateUser(id, userdata, next) {
        if (id && userdata) {
            userdata = JSON.parse(userdata)
            db.models.user.findOne({ where: { id: id } }).then(user => {
                if (userdata.firstName) user.set('firstName',  userdata.firstName);
                if (userdata.lastName) user.set('lastName',  userdata.lastName);
                if (userdata.username) user.set('username',  userdata.username);
                console.log(userdata.username);
                user.save().then(() => next(null)).catch(err => next(err));
            }).catch(err => {
                next(err);
            });
        }
        return null;
    },
    /*
    * Delete the user 
    */
    deleteUser(id, next) {
        if (id) {
            db.models.user.destroy({ where: { id: id } }).then(newUser => {
                next(null);
            }).catch(err => {
                next(err);
            });
        }
    },
}

