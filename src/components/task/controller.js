var db = null;

module.exports = {
    loadModel: function (req, res, next) {
        if (!db) db = res.locals.db.models.task;
        next();
    }, 
    getTasks: function (authorId, projectId) {
        return db.findAll({ where: { authorid: authorId } })
    },
    getTask: function (authorId, projectId, taskId) {
        return db.findOne({ where: { id: taskId, authorid: authorId } })
    },
    updateTask: function (authorId, projectId, taskId, data) {
        return db.update({
            title: data.title,
            description: data.description,
            status: data.status
        }, { where: { id: taskId, authorid: authorId, projectid: projectId } });
    },
    addNewTask: function (authorId, projectId, data) {
        return db.create({
            title: data.title,
            description: data.description,
            status: 'O',
            authorid: authorId, 
            projectid: projectId
        });
    }, 
    deleteTask: function(authorId, projectId, taskId) {
        return db.destroy({ where: { authorid: authorId,  id: taskId, projectid: projectId } }); 
        
    } 
}

