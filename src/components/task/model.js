/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('task', {
		id: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true, 
			autoIncrement: true
		},
		title: {
			type: DataTypes.TEXT,
			allowNull: true
		},
		description: {
			type: DataTypes.TEXT,
			allowNull: true
		},
		creationdate: {
			type: DataTypes.DATE,
			allowNull: true
		},
		status: {
			type: DataTypes.ENUM("O","C"),
			allowNull: true
		},
		projectid: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'project',
				key: 'id'
			}
		},
		authorid: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'user',
				key: 'id'
			}
		}
	}, {
		tableName: 'task',
		timestamps: false
	});
};
