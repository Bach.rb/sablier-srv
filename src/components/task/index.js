var express = require('express');
var router = express.Router({ mergeParams: true });
var controller = require('./controller');

// Middleware
router.use(controller.loadModel);

// Récupérer toutes les tâches 
router.get('/', function (req, res, next) {
    if (req.params.projectId) {
        controller.getTasks(res.locals.currentUserId)
            .then(result => {
                res.status(200).send(result);
            })
            .catch(err => next(new Error('Error when retrieving tasks')));
    }
    else
        next(new Error('Error when retrieving TASK'));
});

// Récupérer une tâche
router.get('/:taskId', function (req, res, next) {
    if (req.params.projectId && req.params.taskId) {
        controller.getTask(res.locals.currentUserId, req.params.projectId, req.params.taskId)
            .then(result => {
                res.status(200).send(result);
            })
            .catch(err => next(new Error('Error when retrieving tasks')));
    }
    else
        next(new Error('Error when creating TASK'));
});

// Ajouter une tâche 
router.put('/', function (req, res, next) {
    if (req.body.data && req.params.projectId) {
        var data = JSON.parse(req.body.data);
        var projectId = req.params.projectId;
        controller.addNewTask(res.locals.currentUserId, projectId, data)
            .then(() => {
                res.status(200).send('Added');
            })
            .catch(err => next(new Error('Error when creating task')));
    }
    else
        next(new Error('Error when creating TASK'));
});


// Modifier une tâche
router.patch('/:taskId', function (req, res, next) {
    if (req.params.projectId && req.params.taskId && req.body.data) {
        var data = JSON.parse(req.body.data);
        controller.updateTask(res.locals.currentUserId, req.params.projectId, req.params.taskId, data)
            .then(() => {
                res.status(200).send('Updated');
            })
            .catch(err => next(new Error('Error when updating task')));
    }
    else
        next(new Error('Error when updating task'));
});

// Supprimer une tâche
router.delete('/:taskId', function (req, res, next) {
    if (req.params.projectId && req.params.taskId) {
        controller.deleteTask(res.locals.currentUserId, req.params.projectId, req.params.taskId)
            .then(() => {
                res.status(200).send('Deleted');
            })
            .catch(err => next(new Error('Error when deleting task')));
    }
    else
        next(new Error('Error when deleting TASK'));
});


module.exports = router;