const express = require("express");
const bodyparser = require("body-parser");
const cookieParser = require('cookie-parser');

const port = process.env.PORT || 4000;
const db = require('./src/services/database.js');
const auth = require('./src/services/auth.js')
const app = express();

// middleware
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(db.init);

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    // allow preflight
    if (req.method === 'OPTIONS') {
        res.send(200);
    } else {
        next();
    }
});

// Routers 
app.use("/resources", require('./src/components'));

// Routes
app.listen(port, () => {
    console.log(`running at port ${port}`);
});

// Login
app.post("/signin", (req, res, next) => {
            var username = req.body.username;
            var passwd = req.body.passwd;
            if (username && passwd) {
                res.locals.db.query("SELECT * FROM public.user where username = :u OR email = :u",
                    {
                        replacements: {
                            u: username
                        },
                        type: res.locals.db.QueryTypes.SELECT
                    })
                    .then(user => {
                        if (user[0]) {
                            auth.checkPassword(passwd, user[0].passwd).then(result => {
                                if (result) {
                                    auth.getToken(user[0], (err, token) => {
                                        res.clearCookie('sessid');
                                        res.cookie('sessid', token);
                                        res.send('Access authorized');
                                    });
                                }
                                else
                                    res.send('Username or password incorrect');
                            }).catch(err => res.send('Username or password incorrect'));
                        }
                        else
                            res.send('Username or password incorrect');
                    }).catch(err => res.send('Can\'t access the remote server, please try later'));

            }
            else
                res.send('No parameters submitted');
});

// Subscribe 
app.post("/subscribe", (req, res, next) => {
            var email = req.body.email;
            var passwd = req.body.passwd;
            if (email && passwd) {
                var hashData = auth.hashPassword(passwd, function (hashData) {
                    res.locals.db.query("INSERT INTO public.user (email, passwd, salt) values (:e, :h, :s)",
                        {
                            replacements: {
                                e: email,
                                h: hashData.hash,
                                s: hashData.salt
                            },
                            type: res.locals.db.QueryTypes.INSERT
                        })
                        .then(() => res.send('Successfully suscribed'))
                        .catch(err => res.send('Error'));
                })

            }
            else
                res.send('No parameters submitted');
});

// Log out
app.post("/signout", (req, res, next) => {
    res.clearCookie('sessid');
    res.send('Logged out');
});

app.use(function (err, req, res, next) {
    res.status(500).send("Error: " + err.message);
});

